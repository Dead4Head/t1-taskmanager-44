package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.amsmirnov.taskmanager.api.repository.model.ISessionRepository;
import ru.t1.amsmirnov.taskmanager.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        final String jql = "SELECT s FROM Session s ORDER BY s.created";
        return entityManager.createQuery(jql, Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAllSorted(@Nullable final String sort) {
        if (sort == null || sort.isEmpty()) return findAll();
        final String jql = "SELECT s FROM Session s ORDER BY :sort";
        return entityManager.createQuery(jql, Session.class)
                .setParameter("sort", sort)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        final String jql = "SELECT s FROM Session s WHERE s.user.id = :userId ORDER BY s.created";
        return entityManager.createQuery(jql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAllSorted(
            @NotNull final String userId,
            @Nullable final String sort
    ) {
        if (sort == null || sort.isEmpty()) return findAll(userId);
        final String jql = "SELECT s FROM Session s WHERE s.user.id = :userId ORDER BY :sort";
        return entityManager.createQuery(jql, Session.class)
                .setParameter("userId", userId)
                .setParameter("sort", sort)
                .getResultList();
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String id) {
        final String jql = "SELECT s FROM Session s WHERE s.id = :id";
        return entityManager.createQuery(jql, Session.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Nullable
    @Override
    public Session findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        if (userId.isEmpty() || id.isEmpty()) return null;
        @NotNull final String jql = "SELECT s FROM Session s WHERE s.user.id = :userId AND s.id = :id";
        return entityManager.createQuery(jql, Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);

    }

    @Override
    public void removeAll() {
        final String jql = "DELETE FROM Session";
        entityManager.createQuery(jql, Session.class).executeUpdate();
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        final String jql = "DELETE FROM Session s WHERE s.user.id = :userId";
        entityManager.createQuery(jql, Session.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
